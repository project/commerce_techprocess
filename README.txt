ABOUT
-----

Commerce Techprocess module is a payment gateway module for techprocess payment gateway
This module only creates a payment transaction object with transaction status and remote transaction ID

We can use commerce checkout rules for successful order status change and mail sending.

Installation
------------
1. Extract the tar.gz into your 'modules' or directory.
2. Enable the module at 'administer >> modules'.
3. Configure options in Administration >> Configuration >>
   Web services >> Configure Tech Process payment method

Configuration
-------------
1. Visit the configuration page at:
   'Administration >> Configuration >>
   Web services >> Configure Tech Process payment method'
2. Configure the form with the details provided by tech process team like Merchant ID,
   Encryption Key, Encryption IV, Scheme Code etc.

Uninstallation
--------------
1. Disable the module.
2. Uninstall the module

Credits
-------
Written by Zyxware, http://www.zyxware.com/
